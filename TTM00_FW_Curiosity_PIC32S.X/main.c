/*
 * @File:   main.c
 * @Author: MARIA CAMILA CALDERON C
 * @Version V1.0
 * 
 * 
 *
 * Created on July 10, 2019, 3:45 AM
 */

#include "main.h"
#include <stdint.h> 
#include <plib.h>
#include <stdio.h>

//Add work frequency

#define SYS_FREQ (96000000UL)
#define PBCLK_FREQUENCY (96 * 1000 * 1000)
#define TOGGLES_PER_SEC 1000 //1ms
#define PRESCALE       8
#define T1_TICK_RATE (PBCLK_FREQUENCY/PRESCALE/TOGGLES_PER_SEC)


// PORT Pin Configuration with Change Notice Interrupts 
#define CONFIG (CND_ON | CND_IDLE_CON)
#define PINS (CND6_ENABLE)
#define PULLUPS (CND_PULLUP_DISABLE_ALL)
#define INTERRUPT (CHANGE_INT_ON | CHANGE_INT_PRI_2)



enum{
    LED_OFF,
    LED_ON
};

enum{
    LED_1,
    LED_2,
    LED_3
};

volatile uint16_t vu16_LED_millis = 1000;
volatile uint16_t vu16_UART_millis = 1500;
//This flag indicate if the buttom 6 are pressed 
volatile uint16_t vu16_SW1_millis = 0;
volatile uint16_t vu16_SW12_millis = 0;
//This flag indicate the Blinking Led status, and return if Blink is executing or no
volatile uint16_t LED2_blink_status = 0; 
//ms total time on
volatile uint16_t vu16_LED_pwm_on =10; 
//ms total time (period)
volatile uint16_t vu16_LED_pwm_period = 20; 
volatile uint16_t vu16_MACRO_PERIOD_PWM=500;
volatile uint16_t vu16_MACRO_ON_PWM=300;


void LED_toggle(uint8_t LED);
void LED_on(uint8_t LED);
void LED_off(uint8_t LED);
void uart_setup(void);

//Structure create
typedef struct {
    uint8_t u8_LED_pin; //POCO
    uint8_t c_LED_port; //PUERTO 
    uint8_t u8_LED_status; // LED encendido o apagado
} 
led_struct_t ;
//Structure instances
led_struct_t LED1_struct;
led_struct_t LED2_struct;
led_struct_t LED3_struct;

void uart_setup(void){
    
    UART1_init (115200);
    
    UART1_write_string ("[PIC]: Initializing ... \r \n");
}

//Data
#define FW_VER "1.0"
#define AUTHOR "Maria Camila Calder�n C"
#define HW_VER "___"


void main(void) {
    
    printf("Booting" FW_VER "-" HW_VER "-" AUTHOR);
    
    uart_setup();
    
    OpenTimer1(T1_ON | T1_PS_1_8, T1_TICK_RATE);
    ConfigIntTimer1((T1_INT_ON | T1_INT_PRIOR_3 | T1_INT_SUB_PRIOR_3));

    
    //Digital outputs  and set LEDs
    mPORTESetPinsDigitalOut(BIT_4|BIT_6|BIT_7);
    mPORTEClearBits(BIT_4|BIT_6|BIT_7);
    //RD6 -> SW1 -> IN
    mPORTDSetPinsDigitalIn(BIT_6);  
    
    unsigned short value;
    PORTSetPinsDigitalOut(IOPORT_B, BIT_3);
    PORTSetPinsDigitalIn(IOPORT_D, BIT_6);
        
    mPORTBSetBits(BIT_3); 
    mCNDOpen(CONFIG, PINS, PULLUPS);
    value = mPORTDRead();
    ConfigIntCND(INTERRUPT);
    INTEnableSystemMultiVectoredInt();
   
    
    while(1){
        //use of the functions according to the status of bit 6
        if(!PORTDbits.RD6)
        {
           //Time counter of buttom 6 was pressed for for LED2 blinking  
          if(vu16_SW12_millis>=2000)
            {
                //SW1 for LED2 Blinking control (STAR STOP)
                if(LED2_blink_status==1)
                {
                    
                 LED2_blink_status=0;
                 
                 vu16_SW12_millis = 0;
                 
                }
                else if(LED2_blink_status==0)
                {
                    
                 LED2_blink_status=1;
                 
                 vu16_SW12_millis = 0;
                 
                }
            }
            //Time counter of buttom 6 was pressed for LED1 Toggle control
            if(vu16_SW1_millis >= 500)
            {
                LED_toggle(LED_1); 
                
                vu16_SW1_millis = 0;
            }  
            
        } //if(!PORTDbits.RD6)
        //"TICK" message sending with control with the UART timer
        if(!vu16_UART_millis)
        {
            UART1_write_string ("TICK \r \n");
            
            vu16_UART_millis = 1500;
        }
        //HEARTBEAT control of led 2 with blink status flag and timer counter LED millis
        if(!vu16_LED_millis && LED2_blink_status==1)
        {
            vu16_LED_millis = 1000;
            
            LED_toggle(LED_2);
        }
        //PWM and heartbeat control 
        if(!vu16_MACRO_ON_PWM && vu16_MACRO_PERIOD_PWM)
        {
            //led off control with the pwm period and the led on timer
            if(!vu16_LED_pwm_on && vu16_LED_pwm_period)
            {
                LED_off(LED_3);
            }
            //reset control of the micro period and micro on count variables
            if(!vu16_LED_pwm_period)  
            {
                vu16_LED_pwm_period=20;
                
                vu16_LED_pwm_on=10;
                
                LED_on(LED_3);
            }
        }
        //reset control of the macro period and macro on count variables
        if(!vu16_MACRO_PERIOD_PWM)
        {
            vu16_MACRO_ON_PWM=300;
            
            vu16_MACRO_PERIOD_PWM=500;
        }
    }//while(1)
 }//void 
        
 

//LED ON Function
void LED_on(uint8_t LED){
    switch (LED){
        case LED_1:
            
            mPORTESetBits(BIT_4);
            
            break;
            
        case LED_2:
            
            mPORTESetBits(BIT_6);
            
            break;
            
        case LED_3:
            
            mPORTESetBits(BIT_7);
            
            break;
            
        default:
            break;
    }
}

//LED OFF Function
void LED_off(uint8_t LED){
    switch (LED) {
        case LED_1:
            
            mPORTEClearBits(BIT_4);
            
            break;
            
        case LED_2:
            
            mPORTEClearBits(BIT_6);
            
            break;
            
        case LED_3:
            
            mPORTEClearBits(BIT_7);
            
            break;
            
        default:
            break;
    }
}

//LED Toggle function
void LED_toggle(uint8_t LED){
    switch (LED){
        case LED_1:
            
            mPORTEToggleBits(BIT_4);
            
            break;
            
        case LED_2:
            
            mPORTEToggleBits(BIT_6);
            
            break;
            
        case LED_3:
            
            mPORTEToggleBits(BIT_7);
            
            break;
            
        default:
            break;
    }
}

void __ISR(_TIMER_1_VECTOR, ipl3) Timer1Handler(void){
    
    mT1ClearIntFlag();
    // update the period
    WritePeriod1(T1_TICK_RATE);
    
    //mPORTEToggleBits(BIT_6|BIT_7);
    if(vu16_LED_millis) vu16_LED_millis--;
    
    if(vu16_UART_millis) vu16_UART_millis--;
    
    if(!PORTDbits.RD6) 
    {
        vu16_SW12_millis++;
        vu16_SW1_millis++; 
        
    }
    //PWM Conditions
    if(vu16_LED_pwm_period) vu16_LED_pwm_period--;
    if(vu16_LED_pwm_period) vu16_LED_pwm_on--;
    if(vu16_MACRO_PERIOD_PWM) vu16_MACRO_PERIOD_PWM--;
    if(vu16_MACRO_ON_PWM) vu16_MACRO_ON_PWM--;
 
}

void __ISR(_CHANGE_NOTICE_VECTOR, ipl2) ChangeNotice_Handler(void)
{
 mPORTDRead();
 mCNDClearIntFlag();
 mPORTBToggleBits(BIT_3);
}


